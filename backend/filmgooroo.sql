-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: filmgooroo
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `follow`
--

DROP TABLE IF EXISTS `follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_follow` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follow`
--

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;
INSERT INTO `follow` VALUES (1,3,2,'2018-05-03 07:04:31','2018-05-03 07:04:31'),(2,3,1,'2018-05-03 07:05:24','2018-05-03 07:05:24'),(10,1,3,'2018-05-03 07:12:20','2018-05-03 07:12:20'),(13,1,5,'2018-05-08 10:35:37','2018-05-08 10:35:37'),(14,1,6,'2018-05-08 10:49:47','2018-05-08 10:49:47'),(21,2,7,'2018-05-09 12:35:09','2018-05-09 12:35:09'),(22,7,2,'2018-05-09 12:37:05','2018-05-09 12:37:05'),(23,7,5,'2018-05-09 12:52:27','2018-05-09 12:52:27'),(25,7,1,'2018-05-09 12:52:39','2018-05-09 12:52:39'),(28,7,6,'2018-05-09 12:53:55','2018-05-09 12:53:55'),(29,7,3,'2018-05-09 12:54:00','2018-05-09 12:54:00'),(63,2,6,'2018-05-10 07:59:37','2018-05-10 07:59:37'),(66,2,1,'2018-05-10 08:00:35','2018-05-10 08:00:35'),(68,1,2,'2018-05-10 08:01:25','2018-05-10 08:01:25'),(69,1,4,'2018-05-10 08:03:22','2018-05-10 08:03:22'),(70,2,3,'2018-05-10 09:00:21','2018-05-10 09:00:21'),(71,1,7,'2018-05-12 13:22:08','2018-05-12 13:22:08'),(72,2,4,'2018-05-15 07:09:44','2018-05-15 07:09:44'),(74,6,7,'2018-05-16 12:28:43','2018-05-16 12:28:43'),(75,2,5,'2018-05-16 14:08:58','2018-05-16 14:08:58'),(76,1,8,'2018-05-18 06:20:17','2018-05-18 06:20:17');
/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lavori`
--

DROP TABLE IF EXISTS `lavori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavori` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria_lavoro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipologia_lavoro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_girato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copertina_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copertina_img_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descrizione` text COLLATE utf8_unicode_ci,
  `nome_regista` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_inizio_lavori` date DEFAULT NULL,
  `data_fine_lavori` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lavori`
--

LOCK TABLES `lavori` WRITE;
/*!40000 ALTER TABLE `lavori` DISABLE KEYS */;
INSERT INTO `lavori` VALUES (1,3,'lavoro test','','','','https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_copertina/25bbbcc7e52d675fa4a6377123586c16e2fb64919ec9299bc784f32f831b.jpeg','lavori_copertina/25bbbcc7e52d675fa4a6377123586c16e2fb64919ec9299bc784f32f831b.jpeg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem quis nibh consequat maximus. Donec molestie mauris elementum urna vestibulum cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet suscipit velit. Pellentesque eget risus id neque ornare congue vel nec lacus.','','2018-05-01','2018-05-02','2018-05-03 07:07:03',NULL),(2,2,'esempio','aerial_photography','spot','','https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_copertina/0ea9a170d3d1e140f625761d57649ac3f161bc7a5d82da08e1c774ede62a.jpeg','lavori_copertina/0ea9a170d3d1e140f625761d57649ac3f161bc7a5d82da08e1c774ede62a.jpeg','descrizione','','2018-04-10','2018-04-17','2018-05-03 07:17:49','2018-05-03 07:29:52'),(3,6,'BNL POSitivity - Website','','','https://www.youtube.com/watch?v=4eTlrAUNxuY','https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_copertina/2970560d953e0ab5b4072a778cd82f06b010f085ddfd229801b7f3b708b3.jpeg','lavori_copertina/2970560d953e0ab5b4072a778cd82f06b010f085ddfd229801b7f3b708b3.jpeg','Soluzioni di pagamento per il tuo business Ti aiutiamo ogni giorno a far crescere il tuo business offrendoti la soluzione di pagamento più adatta alle tue esigenze, con la solidità garantita dal gruppo bancario internazionale BNP Paribas, presente in 75 paesi, tra i leader Europei dei servizi bancari e finanziari','Nicola Camillo','2017-08-01','2018-04-01','2018-05-08 08:42:00',NULL),(4,2,'lavoro one ','camera_grip_e_lighting','','','https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_copertina/84fad023d44b3c1e93457055b2f87f01dfd67e50b399d3ace58edc8fce93.jpeg','lavori_copertina/84fad023d44b3c1e93457055b2f87f01dfd67e50b399d3ace58edc8fce93.jpeg','descrizione del progetto ','','2018-05-02','2018-05-07','2018-05-09 12:15:43','2018-05-15 07:42:58'),(5,6,'HomeVoyage','','','https://www.youtube.com/watch?v=Xuw33mzknqY','https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_copertina/4010c22dd433bad3bf1de7e15e316177a113eafde25c79a69fe990c0ecd2.jpeg','lavori_copertina/4010c22dd433bad3bf1de7e15e316177a113eafde25c79a69fe990c0ecd2.jpeg','HomeVoyage è una innovativa app di viaggio, che permette agli utenti di alloggiare nelle principali capitali del mondo in maniera gratuita. Il funzionamento è semplice e veloce, ti basterà caricare il tuo alloggio in pochi passaggi. Rendendolo disponibile per altri viaggiatori riceverai 35€ per notte, allo stesso tempo sarai libero di prenotare in maniera completamente gratuita tutti gli alloggi presenti nell’app. Se invece non desideri inserire il tuo alloggio HomeVoyage ti da la possibilità di accedere al servizio, pagando una Fee mensile.','Davide Balistreri','2018-04-02','2018-05-10','2018-05-16 12:22:51','2018-05-16 12:25:18');
/*!40000 ALTER TABLE `lavori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lavori_feedback`
--

DROP TABLE IF EXISTS `lavori_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavori_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lavoro` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `titolo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recensione` text COLLATE utf8_unicode_ci,
  `general_attitude` double(4,1) DEFAULT NULL,
  `problem_solving` double(4,1) DEFAULT NULL,
  `budget` double(4,1) DEFAULT NULL,
  `crew` double(4,1) DEFAULT NULL,
  `feedback_totale` double(4,1) DEFAULT NULL,
  `feedback_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `feedback_img_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lavori_feedback`
--

LOCK TABLES `lavori_feedback` WRITE;
/*!40000 ALTER TABLE `lavori_feedback` DISABLE KEYS */;
INSERT INTO `lavori_feedback` VALUES (1,4,7,'recensione a stefanino','ottima recensione !',4.0,5.0,2.5,5.0,4.0,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_feedback/1d3fd2d2f7ff7f759cab05b60d7ab2a069b7429fddbecd99e18d660feaeb.jpeg','lavori_feedback/1d3fd2d2f7ff7f759cab05b60d7ab2a069b7429fddbecd99e18d660feaeb.jpeg','2018-05-09 12:43:35','2018-05-09 12:43:35');
/*!40000 ALTER TABLE `lavori_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lavori_img`
--

DROP TABLE IF EXISTS `lavori_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavori_img` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_lavoro` int(11) NOT NULL,
  `img_lavoro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_lavoro_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordine` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lavori_img`
--

LOCK TABLES `lavori_img` WRITE;
/*!40000 ALTER TABLE `lavori_img` DISABLE KEYS */;
INSERT INTO `lavori_img` VALUES (1,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/dd89ff532c60f0ab7d51ec371fce7734d737a320f744bbadaa27fb1681287679a391cae74b0018f9.jpeg','dd89ff532c60f0ab7d51ec371fce7734d737a320f744bbadaa27fb1681287679a391cae74b0018f9','108623','lavori_photogallery/dd89ff532c60f0ab7d51ec371fce7734d737a320f744bbadaa27fb1681287679a391cae74b0018f9.jpeg',NULL,'2018-05-03 07:07:03',NULL),(2,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/f077671b5e758b118b18c0d22017a0ad89915a81c99b93b1f480f1dc6b6e1683dba3d1bf7c536764.jpeg','f077671b5e758b118b18c0d22017a0ad89915a81c99b93b1f480f1dc6b6e1683dba3d1bf7c536764','43431','lavori_photogallery/f077671b5e758b118b18c0d22017a0ad89915a81c99b93b1f480f1dc6b6e1683dba3d1bf7c536764.jpeg',NULL,'2018-05-03 07:07:03',NULL),(3,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/fc5344e9fe9aed967b6599a64e958f35eee5e7b97241002b6c945a3847d4dd51d02d368824ba4589.jpeg','fc5344e9fe9aed967b6599a64e958f35eee5e7b97241002b6c945a3847d4dd51d02d368824ba4589','188591','lavori_photogallery/fc5344e9fe9aed967b6599a64e958f35eee5e7b97241002b6c945a3847d4dd51d02d368824ba4589.jpeg',NULL,'2018-05-03 07:07:03',NULL),(4,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/2a47177c495c9856f46e48282e32792657dd2af60441cd1aaf5661ad58e73fb1f5c554957c356403.jpeg','2a47177c495c9856f46e48282e32792657dd2af60441cd1aaf5661ad58e73fb1f5c554957c356403','262089','lavori_photogallery/2a47177c495c9856f46e48282e32792657dd2af60441cd1aaf5661ad58e73fb1f5c554957c356403.jpeg',NULL,'2018-05-03 07:07:03',NULL),(5,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/bbdddcb91fcf5b4a1d5e42ec7e5254ccd84ca10125c9f6f1504f4656784428102481c4406a6d9c21.jpeg','bbdddcb91fcf5b4a1d5e42ec7e5254ccd84ca10125c9f6f1504f4656784428102481c4406a6d9c21','28983','lavori_photogallery/bbdddcb91fcf5b4a1d5e42ec7e5254ccd84ca10125c9f6f1504f4656784428102481c4406a6d9c21.jpeg',NULL,'2018-05-03 07:07:03',NULL),(6,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/41fbe7a9ef4a225c888b83562c358cafe059f4cc22343232fabe1feae11c02e79c0e6a1ea8785a85.jpeg','41fbe7a9ef4a225c888b83562c358cafe059f4cc22343232fabe1feae11c02e79c0e6a1ea8785a85','38877','lavori_photogallery/41fbe7a9ef4a225c888b83562c358cafe059f4cc22343232fabe1feae11c02e79c0e6a1ea8785a85.jpeg',NULL,'2018-05-03 07:07:03',NULL),(7,3,1,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/b463b4487d6577d783b8fc49db8a18e041dfd9a8e1b5690cd09b4b94b614365f15c4ee593d3b5b12.jpeg','b463b4487d6577d783b8fc49db8a18e041dfd9a8e1b5690cd09b4b94b614365f15c4ee593d3b5b12','118812','lavori_photogallery/b463b4487d6577d783b8fc49db8a18e041dfd9a8e1b5690cd09b4b94b614365f15c4ee593d3b5b12.jpeg',NULL,'2018-05-03 07:07:03',NULL),(8,2,2,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/9e3e71b2c326c7ecda8a253e7a6975d6416d752f7b4194ab033ab42854986899939108395af02f35.jpeg','9e3e71b2c326c7ecda8a253e7a6975d6416d752f7b4194ab033ab42854986899939108395af02f35','188591','lavori_photogallery/9e3e71b2c326c7ecda8a253e7a6975d6416d752f7b4194ab033ab42854986899939108395af02f35.jpeg',NULL,'2018-05-03 07:17:49',NULL),(9,2,2,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/c6738d6ba2e053cd570c9e6aa8c496460fb0fd4f6ece2d51ea2a9fb60229d0181fa86a1d482b89f8.jpeg','c6738d6ba2e053cd570c9e6aa8c496460fb0fd4f6ece2d51ea2a9fb60229d0181fa86a1d482b89f8','262089','lavori_photogallery/c6738d6ba2e053cd570c9e6aa8c496460fb0fd4f6ece2d51ea2a9fb60229d0181fa86a1d482b89f8.jpeg',NULL,'2018-05-03 07:17:49',NULL),(10,2,2,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/3106cff9f8d48a4ee02f074f49a8019466c5fc03ac1e7e4422241bb2c37d771931d2e53efca841b9.jpeg','3106cff9f8d48a4ee02f074f49a8019466c5fc03ac1e7e4422241bb2c37d771931d2e53efca841b9','28983','lavori_photogallery/3106cff9f8d48a4ee02f074f49a8019466c5fc03ac1e7e4422241bb2c37d771931d2e53efca841b9.jpeg',NULL,'2018-05-03 07:17:49',NULL),(11,2,2,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/22da0c92a8b9ae6efc3846d4c5329a67821760cb3ccfa4205feab7ee5b66f2621517e4d3af24b220.jpeg','22da0c92a8b9ae6efc3846d4c5329a67821760cb3ccfa4205feab7ee5b66f2621517e4d3af24b220','38877','lavori_photogallery/22da0c92a8b9ae6efc3846d4c5329a67821760cb3ccfa4205feab7ee5b66f2621517e4d3af24b220.jpeg',NULL,'2018-05-03 07:17:49',NULL),(12,6,3,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/c50bcd0d9da65075868db71584eec8eddf4796c824a47e498127fdabbe836da0265c20022ac7ce28.png','c50bcd0d9da65075868db71584eec8eddf4796c824a47e498127fdabbe836da0265c20022ac7ce28','77494','lavori_photogallery/c50bcd0d9da65075868db71584eec8eddf4796c824a47e498127fdabbe836da0265c20022ac7ce28.png',NULL,'2018-05-08 08:42:00',NULL),(13,6,3,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/a2650b0960d69caafc30812d43a9f2b95a99f51b1a2c6daf635ebc89051c21bf1f137e5aee18e542.png','a2650b0960d69caafc30812d43a9f2b95a99f51b1a2c6daf635ebc89051c21bf1f137e5aee18e542','38131','lavori_photogallery/a2650b0960d69caafc30812d43a9f2b95a99f51b1a2c6daf635ebc89051c21bf1f137e5aee18e542.png',NULL,'2018-05-08 08:42:00',NULL),(14,6,3,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/91945d0e94b11fd7dc8e95ee5d01ace6aa67aada264e23e4b18ece35e6d70c08db86437d56c88df9.png','91945d0e94b11fd7dc8e95ee5d01ace6aa67aada264e23e4b18ece35e6d70c08db86437d56c88df9','42969','lavori_photogallery/91945d0e94b11fd7dc8e95ee5d01ace6aa67aada264e23e4b18ece35e6d70c08db86437d56c88df9.png',NULL,'2018-05-08 08:42:00',NULL),(15,6,3,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/1e457201de142e8bf2275d4df0e532cfe5180997fceba420b630761fe14ac2b60a13c0edde33fc61.png','1e457201de142e8bf2275d4df0e532cfe5180997fceba420b630761fe14ac2b60a13c0edde33fc61','51757','lavori_photogallery/1e457201de142e8bf2275d4df0e532cfe5180997fceba420b630761fe14ac2b60a13c0edde33fc61.png',NULL,'2018-05-08 08:42:00',NULL),(16,6,3,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/ed720b533199fb504e751ce627a3470c0e303f41b02a42ba5dab5e5b57497e511f3109c0eb16eb68.png','ed720b533199fb504e751ce627a3470c0e303f41b02a42ba5dab5e5b57497e511f3109c0eb16eb68','36718','lavori_photogallery/ed720b533199fb504e751ce627a3470c0e303f41b02a42ba5dab5e5b57497e511f3109c0eb16eb68.png',NULL,'2018-05-08 08:42:00',NULL),(17,6,3,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/29a05fcd4e85400b60853edce38943aab4110ea00d5c3ec0f983eddebc72cde99300cd5554e9f787.png','29a05fcd4e85400b60853edce38943aab4110ea00d5c3ec0f983eddebc72cde99300cd5554e9f787','75837','lavori_photogallery/29a05fcd4e85400b60853edce38943aab4110ea00d5c3ec0f983eddebc72cde99300cd5554e9f787.png',NULL,'2018-05-08 08:42:00',NULL),(18,2,4,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/926f5c5641939d61fb7b2cb2fe9670351459294f43d41e051cc2ae0ded2037c5af174990fa69fc50.jpeg','926f5c5641939d61fb7b2cb2fe9670351459294f43d41e051cc2ae0ded2037c5af174990fa69fc50','18450','lavori_photogallery/926f5c5641939d61fb7b2cb2fe9670351459294f43d41e051cc2ae0ded2037c5af174990fa69fc50.jpeg',NULL,'2018-05-09 12:15:43',NULL),(19,2,4,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/0822c4020b1fe683ccc68439ecf010793bfe49daddab182a7754d867273c2ccb7edcf7cb7954275e.jpeg','0822c4020b1fe683ccc68439ecf010793bfe49daddab182a7754d867273c2ccb7edcf7cb7954275e','7614','lavori_photogallery/0822c4020b1fe683ccc68439ecf010793bfe49daddab182a7754d867273c2ccb7edcf7cb7954275e.jpeg',NULL,'2018-05-09 12:15:43',NULL),(20,2,4,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/7abf92263f1a6bbd6a21d6fa20404bc2f89c83876b6cdb5b13bfa887cb203f3aed528187280c29ac.jpeg','7abf92263f1a6bbd6a21d6fa20404bc2f89c83876b6cdb5b13bfa887cb203f3aed528187280c29ac','7368','lavori_photogallery/7abf92263f1a6bbd6a21d6fa20404bc2f89c83876b6cdb5b13bfa887cb203f3aed528187280c29ac.jpeg',NULL,'2018-05-09 12:15:43',NULL),(21,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/5272f1c3e651e2e4c214b624244b0b5ebd3caf453c68b93ff26766faff123734f366e9c215c89c06.jpeg','5272f1c3e651e2e4c214b624244b0b5ebd3caf453c68b93ff26766faff123734f366e9c215c89c06','105207','lavori_photogallery/5272f1c3e651e2e4c214b624244b0b5ebd3caf453c68b93ff26766faff123734f366e9c215c89c06.jpeg',NULL,'2018-05-16 12:25:18',NULL),(22,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/97c3b8e6e5f031f90d4964e68ab42f2323758bd45584621c814e8e6f897df8b843b994de71e51996.jpeg','97c3b8e6e5f031f90d4964e68ab42f2323758bd45584621c814e8e6f897df8b843b994de71e51996','196579','lavori_photogallery/97c3b8e6e5f031f90d4964e68ab42f2323758bd45584621c814e8e6f897df8b843b994de71e51996.jpeg',NULL,'2018-05-16 12:25:18',NULL),(23,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/ca0e0b77394edb635a3a67237496066ca008040f37ca5a61c473ba204f080309f2b55136718e672d.jpeg','ca0e0b77394edb635a3a67237496066ca008040f37ca5a61c473ba204f080309f2b55136718e672d','130074','lavori_photogallery/ca0e0b77394edb635a3a67237496066ca008040f37ca5a61c473ba204f080309f2b55136718e672d.jpeg',NULL,'2018-05-16 12:25:18',NULL),(24,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/d8d5b384d4062096ff769310fe7719120f4b858d33d8048fb8b00aed1213a5d7dad5e88945c8c016.jpeg','d8d5b384d4062096ff769310fe7719120f4b858d33d8048fb8b00aed1213a5d7dad5e88945c8c016','130074','lavori_photogallery/d8d5b384d4062096ff769310fe7719120f4b858d33d8048fb8b00aed1213a5d7dad5e88945c8c016.jpeg',NULL,'2018-05-16 12:25:18',NULL),(25,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/c8c5a2bf65aa4c597c0b56960a0c91ac8d4c4a06f5cc8f46630df6980256bc02e9f3d57facd203f2.jpeg','c8c5a2bf65aa4c597c0b56960a0c91ac8d4c4a06f5cc8f46630df6980256bc02e9f3d57facd203f2','126396','lavori_photogallery/c8c5a2bf65aa4c597c0b56960a0c91ac8d4c4a06f5cc8f46630df6980256bc02e9f3d57facd203f2.jpeg',NULL,'2018-05-16 12:25:18',NULL),(26,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/87b729ffdea86d8a81c91885541272e076e820d9c23b4073727587acaff4736bf79e3b196fbe345d.jpeg','87b729ffdea86d8a81c91885541272e076e820d9c23b4073727587acaff4736bf79e3b196fbe345d','170483','lavori_photogallery/87b729ffdea86d8a81c91885541272e076e820d9c23b4073727587acaff4736bf79e3b196fbe345d.jpeg',NULL,'2018-05-16 12:25:18',NULL),(27,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/91842da7b4bc9c3568b049aa80ab7192c73ee90d64e910b042f23de77165a647148c45e09dbfdcf7.jpeg','91842da7b4bc9c3568b049aa80ab7192c73ee90d64e910b042f23de77165a647148c45e09dbfdcf7','173524','lavori_photogallery/91842da7b4bc9c3568b049aa80ab7192c73ee90d64e910b042f23de77165a647148c45e09dbfdcf7.jpeg',NULL,'2018-05-16 12:25:18',NULL),(28,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/3397e8e7aef04eaa6fe664fe06044bd893797442b3385ccdb91e281e26e34552df092128436e63cb.jpeg','3397e8e7aef04eaa6fe664fe06044bd893797442b3385ccdb91e281e26e34552df092128436e63cb','165174','lavori_photogallery/3397e8e7aef04eaa6fe664fe06044bd893797442b3385ccdb91e281e26e34552df092128436e63cb.jpeg',NULL,'2018-05-16 12:25:18',NULL),(29,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/5ceb81b4e9c957305355966c33dbad1acff05b717984363374a7f1a7223f42e4b2691d0f804504f4.jpeg','5ceb81b4e9c957305355966c33dbad1acff05b717984363374a7f1a7223f42e4b2691d0f804504f4','112038','lavori_photogallery/5ceb81b4e9c957305355966c33dbad1acff05b717984363374a7f1a7223f42e4b2691d0f804504f4.jpeg',NULL,'2018-05-16 12:25:18',NULL),(30,6,5,'https://filmgooroo.ale.sviluppo.tech/storage/app/lavori_photogallery/e6b5f81aff3c15312c8dd1f407d142a5df335f673f0b26bb7c31c03c636c6ff1fa364cd23e6fb445.jpeg','e6b5f81aff3c15312c8dd1f407d142a5df335f673f0b26bb7c31c03c636c6ff1fa364cd23e6fb445','104722','lavori_photogallery/e6b5f81aff3c15312c8dd1f407d142a5df335f673f0b26bb7c31c03c636c6ff1fa364cd23e6fb445.jpeg',NULL,'2018-05-16 12:25:18',NULL);
/*!40000 ALTER TABLE `lavori_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lavori_location`
--

DROP TABLE IF EXISTS `lavori_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavori_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_lavoro` int(11) NOT NULL,
  `nome_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `indirizzo_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordine` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lavori_location`
--

LOCK TABLES `lavori_location` WRITE;
/*!40000 ALTER TABLE `lavori_location` DISABLE KEYS */;
INSERT INTO `lavori_location` VALUES (1,3,1,'','',NULL,'2018-05-03 07:07:03',NULL),(5,2,2,'nome location','indirizzo location',NULL,'2018-05-03 07:29:52',NULL),(6,2,2,'secondo nome location','indirizzo 2 location',NULL,'2018-05-03 07:29:52',NULL),(7,2,2,'terzo nome location','indirizzo 3 location',NULL,'2018-05-03 07:29:52',NULL),(8,6,3,'Apptoyou','Via Tiburtina 912',NULL,'2018-05-08 08:42:00',NULL),(9,6,3,'BNL POSitivity','Via Aldobrandeschi 300',NULL,'2018-05-08 08:42:00',NULL),(12,2,4,'Centocelle','via dei castani 23',NULL,'2018-05-15 07:42:58',NULL),(13,2,4,'Centocelle','Via delle palme 45',NULL,'2018-05-15 07:42:58',NULL),(15,6,5,'CASA MIA NON E? CASA TUA','Viale dei consoli 105',NULL,'2018-05-16 12:25:18',NULL);
/*!40000 ALTER TABLE `lavori_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lavori_produzione`
--

DROP TABLE IF EXISTS `lavori_produzione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavori_produzione` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_lavoro` int(11) NOT NULL,
  `nome_produzione` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produzione` int(11) DEFAULT NULL,
  `confermata` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lavori_produzione`
--

LOCK TABLES `lavori_produzione` WRITE;
/*!40000 ALTER TABLE `lavori_produzione` DISABLE KEYS */;
INSERT INTO `lavori_produzione` VALUES (1,6,3,'Apptoyou',5,0,'2018-05-08 08:42:00','2018-05-08 08:42:00'),(4,2,4,'produzione software',7,0,'2018-05-15 07:42:58','2018-05-15 07:42:58'),(5,2,4,'Apptoyou',5,0,'2018-05-15 07:42:58','2018-05-15 07:42:58'),(8,6,5,'produzione software',7,0,'2018-05-16 12:25:18','2018-05-16 12:25:18'),(9,6,5,'Apptoyou',5,0,'2018-05-16 12:25:18','2018-05-16 12:25:18');
/*!40000 ALTER TABLE `lavori_produzione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (27,'2017_11_02_142733_users',1),(28,'2017_12_19_102832_lavori',1),(29,'2018_03_01_140645_lavori_feedback',1),(30,'2018_03_13_171015_follow',1),(31,'2018_03_15_155837_notifiche',1),(32,'2018_03_15_155837_preferiti',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifiche`
--

DROP TABLE IF EXISTS `notifiche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifiche` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `to_user` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `id_tag` int(11) DEFAULT NULL,
  `stato_tag` int(11) NOT NULL DEFAULT '3',
  `descrizione` text COLLATE utf8_unicode_ci,
  `visualizzata` tinyint(1) NOT NULL DEFAULT '0',
  `eliminato` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifiche`
--

LOCK TABLES `notifiche` WRITE;
/*!40000 ALTER TABLE `notifiche` DISABLE KEYS */;
INSERT INTO `notifiche` VALUES (1,3,2,1,NULL,3,'Ha iniziato a seguirti',1,1,'2018-05-03 07:04:31','2018-05-15 07:20:44'),(2,3,1,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-03 07:05:24','2018-05-18 07:00:30'),(3,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:10:52','2018-05-03 07:10:52'),(4,1,2,1,NULL,3,'Ha iniziato a seguirti',1,1,'2018-05-03 07:11:05','2018-05-15 07:20:53'),(5,1,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-03 07:11:16','2018-05-03 07:11:16'),(6,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:12:00','2018-05-03 07:12:00'),(7,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:12:04','2018-05-03 07:12:04'),(8,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:12:08','2018-05-03 07:12:08'),(9,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:12:14','2018-05-03 07:12:14'),(10,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:12:16','2018-05-03 07:12:16'),(11,1,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 07:12:20','2018-05-03 07:12:20'),(12,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-03 10:57:41','2018-05-03 10:57:41'),(13,2,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-04 05:45:14','2018-05-04 05:45:14'),(14,2,1,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-04 05:55:27','2018-05-18 07:00:30'),(15,6,5,4,1,3,'Ti ha taggato come produttore di BNL POSitivity - Website',0,0,'2018-05-08 08:42:00','2018-05-08 08:42:00'),(16,1,5,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-08 10:35:37','2018-05-08 10:35:37'),(17,1,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-08 10:49:47','2018-05-17 13:26:21'),(18,1,6,2,NULL,3,'Ha messo BNL POSITIVITY - WEBSITE tra i preferiti',1,0,'2018-05-08 10:50:14','2018-05-17 13:26:21'),(19,1,2,2,NULL,3,'Ha messo ESEMPIO tra i preferiti',1,0,'2018-05-08 10:51:49','2018-05-17 05:47:47'),(20,2,6,2,NULL,3,'Ha messo BNL POSITIVITY - WEBSITE tra i preferiti',1,0,'2018-05-08 11:51:36','2018-05-17 13:26:21'),(21,2,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-08 12:13:03','2018-05-08 12:13:03'),(22,2,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-08 12:13:14','2018-05-08 12:13:14'),(23,2,6,2,NULL,3,'Ha messo BNL POSITIVITY - WEBSITE tra i preferiti',1,0,'2018-05-08 12:13:32','2018-05-17 13:26:21'),(24,2,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-08 12:20:54','2018-05-08 12:20:54'),(25,2,6,2,NULL,3,'Ha messo BNL POSITIVITY - WEBSITE tra i preferiti',1,0,'2018-05-08 12:20:57','2018-05-17 13:26:21'),(26,2,6,2,NULL,3,'Ha messo BNL POSITIVITY - WEBSITE tra i preferiti',1,0,'2018-05-08 12:21:00','2018-05-17 13:26:21'),(27,2,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-08 12:21:03','2018-05-08 12:21:03'),(28,2,5,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-08 12:27:41','2018-05-08 12:27:41'),(29,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-08 12:27:56','2018-05-17 13:26:21'),(30,2,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-09 05:40:58','2018-05-09 05:40:58'),(31,2,6,2,NULL,3,'Ha messo BNL POSITIVITY - WEBSITE tra i preferiti',1,0,'2018-05-09 05:45:01','2018-05-17 13:26:21'),(32,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 05:55:25','2018-05-17 13:26:21'),(33,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-09 05:56:59','2018-05-09 05:56:59'),(34,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 11:28:45','2018-05-17 13:26:21'),(35,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 11:28:51','2018-05-17 13:26:21'),(36,2,7,4,2,1,'Ti ha taggato come produttore di lavoro one ',1,0,'2018-05-09 12:15:43','2018-05-09 12:40:48'),(37,2,5,4,3,3,'Ti ha taggato come produttore di lavoro one ',0,0,'2018-05-09 12:15:43','2018-05-09 12:15:43'),(38,2,7,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 12:35:09','2018-05-09 12:40:47'),(39,7,2,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 12:37:05','2018-05-17 05:47:47'),(40,7,2,5,NULL,3,'produzione software ha confermato il tuo tag su lavoro one ',1,0,'2018-05-09 12:40:47','2018-05-17 05:47:47'),(41,7,2,3,NULL,3,'Ha lasciato un Feedback a  RECENSIONE A STEFANINO',1,1,'2018-05-09 12:43:35','2018-05-15 08:36:27'),(42,7,5,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-09 12:52:27','2018-05-09 12:52:27'),(43,7,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-09 12:52:34','2018-05-09 12:52:34'),(44,7,1,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 12:52:39','2018-05-18 07:00:30'),(45,7,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 12:52:43','2018-05-17 13:26:21'),(46,7,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 12:53:47','2018-05-17 13:26:21'),(47,7,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-09 12:53:55','2018-05-17 13:26:21'),(48,7,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-09 12:54:00','2018-05-09 12:54:00'),(49,7,3,2,NULL,3,'Ha messo LAVORO TEST tra i preferiti',0,0,'2018-05-09 12:54:11','2018-05-09 12:54:11'),(50,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 06:30:47','2018-05-17 13:26:21'),(51,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 06:30:51','2018-05-17 13:26:21'),(52,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 06:31:09','2018-05-10 06:31:09'),(53,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 06:31:11','2018-05-10 06:31:11'),(54,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 06:31:13','2018-05-10 06:31:13'),(55,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 06:31:18','2018-05-10 06:31:18'),(56,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 06:31:23','2018-05-10 06:31:23'),(57,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 07:11:54','2018-05-10 07:11:54'),(58,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 07:11:56','2018-05-10 07:11:56'),(59,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 07:11:58','2018-05-10 07:11:58'),(60,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 07:12:29','2018-05-10 07:12:29'),(61,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:18:49','2018-05-17 13:26:21'),(62,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:51:46','2018-05-17 13:26:21'),(63,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:52:46','2018-05-17 13:26:21'),(64,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:53:05','2018-05-17 13:26:21'),(65,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:53:12','2018-05-17 13:26:21'),(66,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:54:11','2018-05-17 13:26:21'),(67,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:54:52','2018-05-17 13:26:21'),(68,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:54:55','2018-05-17 13:26:21'),(69,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:54:59','2018-05-17 13:26:21'),(70,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:55:45','2018-05-17 13:26:21'),(71,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:55:48','2018-05-17 13:26:21'),(72,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:55:50','2018-05-17 13:26:21'),(73,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:56:20','2018-05-17 13:26:21'),(74,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:56:50','2018-05-17 13:26:21'),(75,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:56:52','2018-05-17 13:26:21'),(76,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:57:01','2018-05-17 13:26:21'),(77,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:57:59','2018-05-17 13:26:21'),(78,2,6,1,NULL,3,'Ha iniziato a seguirti',1,1,'2018-05-10 07:58:33','2018-05-16 12:48:17'),(79,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:58:35','2018-05-17 13:26:21'),(80,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:58:44','2018-05-17 13:26:21'),(81,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:59:28','2018-05-17 13:26:21'),(82,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:59:32','2018-05-17 13:26:21'),(83,2,6,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 07:59:37','2018-05-17 13:26:21'),(84,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 07:59:43','2018-05-10 07:59:43'),(85,2,1,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 08:00:33','2018-05-18 07:00:30'),(86,2,1,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 08:00:35','2018-05-18 07:00:30'),(87,1,2,1,NULL,3,'Ha iniziato a seguirti',1,0,'2018-05-10 08:01:23','2018-05-17 05:47:47'),(88,1,2,1,NULL,3,'Ha iniziato a seguirti',1,1,'2018-05-10 08:01:25','2018-05-15 08:36:31'),(89,1,4,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 08:03:22','2018-05-10 08:03:22'),(90,2,3,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-10 09:00:21','2018-05-10 09:00:21'),(91,1,7,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-12 13:22:08','2018-05-12 13:22:08'),(92,2,4,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-15 07:09:44','2018-05-15 07:09:44'),(93,1,2,2,NULL,3,'Ha messo LAVORO ONE  tra i preferiti',1,0,'2018-05-15 08:11:17','2018-05-17 05:47:47'),(94,6,7,4,6,3,'Ti ha taggato come produttore di HomeVoyage',0,0,'2018-05-16 12:22:51','2018-05-16 12:22:51'),(95,6,5,4,7,3,'Ti ha taggato come produttore di HomeVoyage',0,0,'2018-05-16 12:22:51','2018-05-16 12:22:51'),(96,6,7,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-16 12:28:41','2018-05-16 12:28:41'),(97,6,7,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-16 12:28:43','2018-05-16 12:28:43'),(98,2,6,2,NULL,3,'Ha messo HOMEVOYAGE tra i preferiti',1,0,'2018-05-16 14:08:45','2018-05-17 13:26:21'),(99,2,5,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-16 14:08:58','2018-05-16 14:08:58'),(100,1,8,1,NULL,3,'Ha iniziato a seguirti',0,0,'2018-05-18 06:20:17','2018-05-18 06:20:17');
/*!40000 ALTER TABLE `notifiche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferiti`
--

DROP TABLE IF EXISTS `preferiti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferiti` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_lavoro` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferiti`
--

LOCK TABLES `preferiti` WRITE;
/*!40000 ALTER TABLE `preferiti` DISABLE KEYS */;
INSERT INTO `preferiti` VALUES (1,1,1,'2018-05-03 07:11:16','2018-05-03 07:11:16'),(3,1,3,'2018-05-08 10:50:14','2018-05-08 10:50:14'),(4,1,2,'2018-05-08 10:51:49','2018-05-08 10:51:49'),(13,2,1,'2018-05-09 05:40:58','2018-05-09 05:40:58'),(14,2,3,'2018-05-09 05:45:01','2018-05-09 05:45:01'),(15,7,1,'2018-05-09 12:54:11','2018-05-09 12:54:11'),(16,1,4,'2018-05-15 08:11:17','2018-05-15 08:11:17'),(17,2,5,'2018-05-16 14:08:45','2018-05-16 14:08:45');
/*!40000 ALTER TABLE `preferiti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipologia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cognome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descrizione` text COLLATE utf8_unicode_ci,
  `telefono` bigint(20) DEFAULT NULL,
  `indirizzo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_civico` int(11) DEFAULT NULL,
  `cap` int(11) DEFAULT NULL,
  `sito_web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imdb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono_referente` bigint(20) DEFAULT NULL,
  `nome_referente` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cognome_referente` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `giorno_nascita` int(11) DEFAULT NULL,
  `mese_nascita` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anno_nascita` int(11) DEFAULT NULL,
  `url_facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_linkedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eliminato` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'jocobueno@gmail.com','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/sxK1eQEiTeGSFzgMMsJExGv9RqDSW51AzVU3VfN1.jpg','https://filmgooroo.ale.sviluppo.tech/avatar/sxK1eQEiTeGSFzgMMsJExGv9RqDSW51AzVU3VfN1.jpg','$2y$10$Mtoej8M1u32EuNVMTOOf1uivPVH.sWse87nzGJ4OZFLsW69tRLLha','Rental Companies, Freelance, Other',0,'Alessandro','Olivieri',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-03 05:39:11','2018-05-03 05:39:11'),(2,'prova@prova.com','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/bfCd3RCrcKFQqBBDCynHeTwOU6tDw3ITGYdQEauc.jpeg','avatar/bfCd3RCrcKFQqBBDCynHeTwOU6tDw3ITGYdQEauc.jpeg','$2y$10$2OAhahFM6CPxShdLAHU5wuNxL8gbI/PtCvl4Mud1yP2Ru4hUWgCaK','Rental Companies, Freelance, Other',1,'stefanino','bocci','benvenuti sul mio profilo',0,NULL,'Roma',NULL,NULL,'','',NULL,NULL,NULL,3,'Aprile',1949,NULL,NULL,NULL,0,NULL,'2018-05-03 05:47:04','2018-05-09 12:14:37'),(3,'marketing@apptoyou.it','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/DgcBZ6RV3OykEgIjm0tGZo1BHaXDKRNMoD8OiHzm.jpg','https://filmgooroo.ale.sviluppo.tech/avatar/DgcBZ6RV3OykEgIjm0tGZo1BHaXDKRNMoD8OiHzm.jpg','$2y$10$98oBY8qWzsjsayma29bPmOJ7bXiFUtfwz.yuspTnWAFckIKoKRgpC','Casting Service',0,'Francesca Appia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-03 07:04:09','2018-05-03 07:04:09'),(4,'m.mappa@apptoyou.it','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/66kAgu2kg0QZzFLOzxq3Pt6qdUuKZ82pBbSMszzl.jpeg','avatar/66kAgu2kg0QZzFLOzxq3Pt6qdUuKZ82pBbSMszzl.jpeg','$2y$10$TUaiQX2MPm1rktpOCJbbe.4VzbCVWqHLIYLK26WzUbU4uo8zB/17a','Rental Companies, Freelance, Other',0,'Michele','Mappa','I\'m Michele and i\'m based in Rome. I\'m specialized in Ux/Ui & Web Design. Always interested in cool works.',3479070070,NULL,'Roma',NULL,NULL,'https://www.behance.net/mikappa','',NULL,NULL,NULL,11,'Febbraio',1991,'https://www.facebook.com/michelemappa.91','https://www.linkedin.com/in/michele-mappa-05a22845/','https://twitter.com/duemme91',0,NULL,'2018-05-07 11:05:20','2018-05-07 11:22:10'),(5,'m.mappa@apptpyou.it','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/lsX4MKdFRfEDaN5OHu4JEYp08vWcZFqxuQ8uW611.jpeg','avatar/lsX4MKdFRfEDaN5OHu4JEYp08vWcZFqxuQ8uW611.jpeg','$2y$10$1lue7vP6hgEcJJ170vAkyOtrPn4EtHpWnqKTkTExKujDDw81bv8si','Production',1,'Apptoyou',NULL,'Vogliamo migliorare la qualità della vita delle persone e delle imprese creando e rendendo disponibili a tutti servizi tecnologici utili e facili da usare.',3589773470,'Via Tiburtina ','Roma',912,156,'http://www.apptoyou.it/','',3479070070,'Michele','Mappa',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-07 11:51:14','2018-05-07 11:54:00'),(6,'2emme91@gmail.com','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/29fMxwseboJsXW30EW4w5I2xoHKoWcJaHmErgTum.jpg','https://filmgooroo.ale.sviluppo.tech/avatar/29fMxwseboJsXW30EW4w5I2xoHKoWcJaHmErgTum.jpg','$2y$10$u94aNx7v5rIQa9W4JUZuQuzcr8wALT1zfYUEumLd2DAFgUYTsPeO2','Rental Companies, Freelance, Other',0,'Michele','Mappa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-08 08:36:14','2018-05-08 08:36:14'),(7,'produzione@prova.com','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/BDRLFQEQzt5aYlfGolWgn6s1wPVPokiBunxNB9g3.png','avatar/BDRLFQEQzt5aYlfGolWgn6s1wPVPokiBunxNB9g3.png','$2y$10$zdweMVbkBUnymvNyUcGFwODsj93eztEKofAav8bkFyGgIiRe6OjOK','Production',1,'produzione software',NULL,'descrizione produzione software',0,'via da e','Roma',34,181,'','',0,'','',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-09 12:10:47','2018-05-09 12:11:48'),(8,'thelastmohican@tiscali.it','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/gWh0EhRjCRfbNFCjWpwq15Cz8LF0YKtbhVLfDZJh.jpg','https://filmgooroo.ale.sviluppo.tech/avatar/gWh0EhRjCRfbNFCjWpwq15Cz8LF0YKtbhVLfDZJh.jpg','$2y$10$kVtQ4Q.ctS2PgGzIOYYU3OKtbrPAQRes7IfJ1DDWRvwcESnF4YJg2','Rental Companies, Freelance, Other',0,'Maurizio','Dionne',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-18 05:46:58','2018-05-18 05:46:58'),(9,'davide.sac@gmail.com','https://filmgooroo.ale.sviluppo.tech/storage/app/avatar/avatar_default.png','avatar/avatar_default.png','$2y$10$VFJJMbLW9IWkGlRDCH0M7ealLfWUIyNz2ZrbMSIbV1s5Mvc/x9KEO','Rental Companies, Freelance, Other',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'2018-05-18 07:04:10','2018-05-18 07:04:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_specializzazione`
--

DROP TABLE IF EXISTS `users_specializzazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_specializzazione` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `specializzazione` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_specializzazione`
--

LOCK TABLES `users_specializzazione` WRITE;
/*!40000 ALTER TABLE `users_specializzazione` DISABLE KEYS */;
INSERT INTO `users_specializzazione` VALUES (13,4,'underwater_photography','2018-05-07 11:17:49',NULL),(14,2,'camera_grip_e_lighting','2018-05-09 12:14:37',NULL),(15,2,'sfx','2018-05-09 12:14:37',NULL),(16,2,'stunts','2018-05-09 12:14:37',NULL),(17,2,'set_dressing_prop_houses','2018-05-09 12:14:37',NULL),(18,2,'costumes','2018-05-09 12:14:37',NULL),(19,2,'aerial_photography','2018-05-09 12:14:37',NULL),(20,2,'make_up_hair','2018-05-09 12:14:37',NULL),(21,2,'underwater_photography','2018-05-09 12:14:37',NULL);
/*!40000 ALTER TABLE `users_specializzazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_tipologia_lavoro_svolto`
--

DROP TABLE IF EXISTS `users_tipologia_lavoro_svolto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_tipologia_lavoro_svolto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `tipologia_lavoro_svolto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_tipologia_lavoro_svolto`
--

LOCK TABLES `users_tipologia_lavoro_svolto` WRITE;
/*!40000 ALTER TABLE `users_tipologia_lavoro_svolto` DISABLE KEYS */;
INSERT INTO `users_tipologia_lavoro_svolto` VALUES (9,4,'spot','2018-05-07 11:17:49',NULL),(10,2,'feature_film','2018-05-09 12:14:37',NULL),(11,2,'spot','2018-05-09 12:14:37',NULL),(12,2,'photo_shoot','2018-05-09 12:14:37',NULL),(13,2,'documentary','2018-05-09 12:14:37',NULL);
/*!40000 ALTER TABLE `users_tipologia_lavoro_svolto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 11:55:13
