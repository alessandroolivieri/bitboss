<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posizioni extends Migration
{


    public function up()
    {
        Schema::create('posizioni', function (Blueprint $table) {
            $table->increments('id');
            $table->string('posizione');
            $table->string('descrizione')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('posizioni');
    }
}
