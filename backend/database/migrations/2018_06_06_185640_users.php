<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{


    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cognome');
            $table->string('password');
            $table->string('email')->unique();
            $table->boolean('is_admin')->default(0);
            $table->boolean('eliminato')->default(0);
            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('users');
    }
}
