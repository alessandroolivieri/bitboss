<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);


$api->version('v1',function (Router $api) { //inizio api v1


    $api->group(['prefix' => 'v1'], function(Router $api) { // inizio gruppo v1


        $api->group(['prefix' => 'auth','middleware' => 'cors'], function(Router $api) { //inizio api auth

            $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');

            $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        }); //fine api auth



        $api->group(['middleware' => ['cors','jwt.auth']], function(Router $api) {//inizio route protetto da token


            $api->get('posizioni/{id?}', 'App\\Api\\V1\\Controllers\\PosizioniController@Get_posizioni');

            $api->post('posizioni', 'App\\Api\\V1\\Controllers\\PosizioniController@Post_posizione');

            $api->post('user', 'App\\Api\\V1\\Controllers\\UserController@Get');

            $api->post('candidati', 'App\\Api\\V1\\Controllers\\CandidatureController@Post_candidatura');

            $api->get('candidature', 'App\\Api\\V1\\Controllers\\CandidatureController@Get_candidature');

            $api->post('candidature', 'App\\Api\\V1\\Controllers\\CandidatureController@Put_candidatura');

            $api->get('candidati', 'App\\Api\\V1\\Controllers\\CandidatureController@Get_candidati');



        }); // fine route protetto da token





    }); // fine gruppo v1





}); //fine api v1
