<?php


namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use Mockery\Exception;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Posizioni;


class PosizioniController extends Controller
{ // inizio classe PosizioniController


    private $user;
    public  $errore;
    public  $data_attuale;

    public function __construct(JWTAuth $JWTAuth) {//inizio costruttore

        $this->errore = array();

        $this->user = $JWTAuth->parseToken()->authenticate();

        $this->data_attuale = date('Y-m-d H:i:s');

    }//fine costruttore


    public function Post_posizione (Request $request) { // inizio Post_posizione

        if ($this->user->is_admin) {

            $posizione = $request->get('posizione') ?? null;
            $descrizione = $request->get('descrizione') ?? '';

            if ($posizione != null) {

                $ritorno = Posizioni::insert([
                    'posizione' => $posizione,
                    'descrizione' => $descrizione,
                    'created_at' => $this->data_attuale,
                    'updated_at' => $this->data_attuale
                ]);

                if ($ritorno) {

                    return response()->json([
                        'status' => 'ok',
                        'info' => 'Operazione completata con successo'
                    ],200);

                } else {

                    return response()->json([
                        'status' => 'error',
                        'info' => 'Errore, operazione non completata'
                    ],400);

                }


            } else {

                return response()->json([
                    'status' => 'error',
                    'info' => 'Errore, invia i campi per continuare'
                ],400);

            }

        } else {

            return response()->json([
                'status' => 'error',
                'info' => 'Errore, non hai i permessi per proseguire'
            ],400);

        }


    } // fine Post_posizione


    public function Get_posizioni ($id = null) { // inizio Get_posizioni

        if ($id != null) {

             $ritorno =  Posizioni::where('id',$id)->first();

            if ($ritorno != null) {

                return response()->json([
                    'status' => 'ok',
                    'dati' => $ritorno
                ],200);

            } else {

                return response()->json([
                    'status' => 'error',
                    'info' => 'Errore, posizione non esistente'
                ],400);

            }

        } else {

            $ritorno =  Posizioni::get();

            return response()->json([
                'status' => 'ok',
                'dati' => $ritorno
            ],200);


        }


    } // fine Get_posizioni





} // fine classe PosizioniController