<?php


namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use Mockery\Exception;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Posizioni;
use App\Model\Candidature;


class CandidatureController extends Controller
{ // inizio classe CandidatureController


    private $user;
    public  $errore;
    public  $data_attuale;

    public function __construct(JWTAuth $JWTAuth) {//inizio costruttore

        $this->errore = array();

        $this->user = $JWTAuth->parseToken()->authenticate();

        $this->data_attuale = date('Y-m-d H:i:s');

    }//fine costruttore


    public function Post_candidatura (Request $request) { // inizio Post_candidatura

        $id_posizione = $request->get('id_posizione') ?? null;

        if ($id_posizione != null) {

            $posizioni_controllo = Posizioni::where('id',$id_posizione)->count();

            if ($posizioni_controllo > 0) {

                $controllo = Candidature::where('id_posizione',$id_posizione)->where('id_utente',$this->user->id)->count();

                if ($controllo < 1) {

                    $ritorno = Candidature::insert([
                       'id_posizione' => $id_posizione,
                       'id_utente' => $this->user->id
                    ]);

                    if ($ritorno) {

                        return response()->json([
                            'status' => 'ok',
                            'info' => 'Operazione completata con successo'
                        ],200);

                    } else {

                        return response()->json([
                            'status' => 'error',
                            'info' => 'Errore, operazione non completata'
                        ],400);

                    }

                } else {

                    return response()->json([
                        'status' => 'error',
                        'info' => 'Errore, ti sei già iscritto'
                    ],400);

                }

            } else {

                return response()->json([
                    'status' => 'error',
                    'info' => 'Errore, la candidatura richiesta non esiste'
                ],400);

            }

        } else {

            return response()->json([
                'status' => 'error',
                'info' => 'Errore, invia i campi per continuare'
            ],400);

        }

    } // fine metodo Post_candidatura

    public function Get_candidature () {


            $ritorno = Candidature::Join('posizioni','candidature.id_posizione','posizioni.id')
                ->select('candidature.id as id_candidatura','posizioni.id as id_posizione','posizioni.posizione','candidature.stato')
                ->where('candidature.id_utente',$this->user->id)
                ->get();

            if ($ritorno != null) {

                return response()->json([
                    'status' => 'error',
                    'dati' => $ritorno
                ],200);

            } else {


                return response()->json([
                    'status' => 'error',
                    'dati' => []
                ],200);
            }

    }


    public function Get_candidati () {


        $ritorno = Candidature::Join('posizioni','candidature.id_posizione','posizioni.id')
            ->Join('users','candidature.id_utente','users.id')
            ->select('users.id as id_user','users.nome','users.cognome','candidature.id as id_candidatura','posizioni.id as id_posizione','posizioni.posizione','candidature.stato')
            ->get();

        if ($ritorno != null) {

            return response()->json([
                'status' => 'error',
                'dati' => $ritorno
            ],200);

        } else {


            return response()->json([
                'status' => 'error',
                'dati' => []
            ],200);
        }

    }


    public function Put_candidatura (Request $request) { // inizio Post_candidatura

        if ($this->user->is_admin) {

            $id_candidatura = $request->get('id_candidatura') ?? null;
            $stato = $request->get('stato') ?? null;

            if ($id_candidatura) {

                $ritorno = Candidature::where('id',$id_candidatura)->update([
                    'stato' => $stato
                ]);

                if ($ritorno != null) {

                    return response()->json([
                        'status' => 'error',
                        'info' => 'Operazione completata con successo'
                    ],200);


                } else {

                    return response()->json([
                        'status' => 'error',
                        'info' => 'Errore, Operazione non conclusa'
                    ],400);

                }

            } else {

                return response()->json([
                    'status' => 'error',
                    'info' => 'Errore, invia i campi per continuare'
                ],400);

            }

        } else {

            return response()->json([
                'status' => 'error',
                'info' => 'Errore, non hai i permessi per proseguire'
            ],400);
        }


    } // fine metodo Post_candidatura









} // fine classe CandidatureController