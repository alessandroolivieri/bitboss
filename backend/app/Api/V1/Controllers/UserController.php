<?php


namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Model\User;
use Mockery\Exception;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Users;



class UserController extends Controller
{ // inizio classe UserController


    private $user;
    public  $errore;
    public  $data_attuale;

    public function __construct(JWTAuth $JWTAuth) {//inizio costruttore

        $this->errore = array();

        $this->user = $JWTAuth->parseToken()->authenticate();

        $this->data_attuale = date('Y-m-d H:i:s');

    }//fine costruttore


    public function Get ()
    { // inizio Get

        $ritorno = User::where('id', $this->user->id)->first();

        return response()->json([
            'status' => 'ok',
            'dati' => $ritorno
        ], 200);

    } // fine Get





} // fine classe UserController