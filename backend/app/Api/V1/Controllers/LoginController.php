<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\Model\User;

class LoginController extends Controller
{


    public function login(LoginRequest $request, JWTAuth $JWTAuth) { // inizio metodo login

        $credentials = $request->only(['email', 'password']);


        try {

            $esistente = User::where('email',$credentials['email'])->select('eliminato')->first();

            if ($esistente == null) {

                return response()->json([
                    'status' => 'error',
                    'info' => 'Accesso non riuscito , email non corretta'
                ],403);

            } else if ($esistente !== null && $esistente->eliminato === 1) {

                return response()->json([
                    'status' => 'error',
                    'info' => 'Account Eliminato'
                ],403);

            } else {
                $token = Auth::guard()->attempt($credentials);

                if(!$token) {

                    return response()->json([
                        'status' => 'error',
                        'info' => 'Accesso non riuscito , password non corretta'
                    ],403);
                }
            }

        } catch (JWTException $e) {

            return response()->json([
                'status' => 'error',
                'info' => 'Accesso non riuscito , controlla Username e Paswword inserite'
            ],500);
        }


        return response()
            ->json([
                'status' => 'ok',
                'token' => $token,
            ],200);



    } // fine metodo login




} // fine classe
