<?php

namespace App\Api\V1\Controllers;

use Config;
use App\Model\User;
use Mockery\Exception;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller {//inzio classe signupController


    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth){//inizio metodo signup

        $array_dati =  $request->all();

        $controllo_email = User::where('email','=',$array_dati['email'])->count();



        if (!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $array_dati['email'])) {

            return response()->json([
                'status' => 'error',
                'info' => 'Email non valida'
            ],400);

        }

        if($controllo_email > 0){//inizio controllo email se gia esistente

            return response()->json([
                'status' => 'error',
                'info' => 'email già esistente'
            ],400);

        }else{//inizio else controllo email se gia esistente

            if(strlen($array_dati['password']) > 5){//inizio if controllo caratteri password

                try {

                    $user = new User($array_dati);
                    if(!$user->save()) {
                        throw new HttpException(500);
                    }

                } catch (Exception $e) {


                    return response()->json([
                        'status' => 'error',
                        'info' => 'errore'
                    ]);
                }


                $token = $JWTAuth->fromUser($user);

                return response()->json([
                    'status' => 'ok',
                    'token' => $token
                ], 200);


            }else{//inizio else controllo caratteri password

                return response()->json([
                    'status' => 'error',
                    'info' => 'password di almeno 6 caratteri'
                ]);


            }//fine else controllo caratteri password

        }//fine else controllo email se gia esistente


    }//fine metodo signup


}//fine classe signup
