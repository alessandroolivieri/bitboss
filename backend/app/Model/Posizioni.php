<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;


class Posizioni extends Model
{ //inizio model Posizioni

    protected $table = 'posizioni';

    protected $fillable = [
        'id',
        'posizione',
        'descrizione',
        'created_at',
        'updated_at'
    ];


} //fine model Posizioni
