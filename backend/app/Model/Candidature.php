<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;


class Candidature extends Model
{ //inizio model Candidature

    protected $table = 'candidature';

    protected $fillable = [
        'id',
        'id_posizione',
        'id_utente',
        'stato',
        'created_at',
        'updated_at'
    ];


} //fine model Candidature
