<?php

namespace App\Http\Kernel;

class saveUserInformazioni { // inizio classe saveUserInformazioni

    public function __construct() { // inizio costruttore

    } // fine costruttore


    public function Put ($dati = null) { // inizio metodo Put

        if ($dati !== null && count($dati) > 0) { // inizio metodo dati


            unset($dati['token']);
            unset($dati['specializzazione']);
            unset($dati['tipologia_lavoro_svolto']);

            $data_finale = [];

            (array_key_exists('nome',$dati) )  ? $data_finale['nome'] = stripslashes($dati['nome']) : null;

            (array_key_exists('cognome',$dati) )  ? $data_finale['cognome'] = stripslashes($dati['cognome']) : null;

            (array_key_exists('descrizione',$dati) ) ? $data_finale['descrizione'] = stripslashes($dati['descrizione']) : null;

            (array_key_exists('telefono',$dati) ) ? $data_finale['telefono'] = stripslashes($dati['telefono']) : null;

            (array_key_exists('indirizzo',$dati) ) ? $data_finale['indirizzo'] = stripslashes($dati['indirizzo']) : null;

            (array_key_exists('citta',$dati) ) ? $data_finale['citta'] = stripslashes($dati['citta']) : null;

            (array_key_exists('numero_civico',$dati) ) ? $data_finale['numero_civico'] = stripslashes($dati['numero_civico']) : null;

            (array_key_exists('cap',$dati) ) ? $data_finale['cap'] = stripslashes($dati['cap']): null;

            (array_key_exists('sito_web',$dati) ) ? $data_finale['sito_web'] = stripslashes($dati['sito_web']) : null;

            (array_key_exists('imdb',$dati)) ? $data_finale['imdb'] = stripslashes($dati['imdb']) : null;

            (array_key_exists('telefono_referente',$dati) ) ? $data_finale['telefono_referente'] = stripslashes($dati['telefono_referente']) : null;

            (array_key_exists('nome_referente',$dati) ) ? $data_finale['nome_referente'] = stripslashes($dati['nome_referente']) : null;

            (array_key_exists('cognome_referente',$dati)) ? $data_finale['cognome_referente'] = stripslashes($dati['cognome_referente']) : null;

            (array_key_exists('giorno_nascita',$dati) ) ? $data_finale['giorno_nascita'] = stripslashes($dati['giorno_nascita']) : null;

            (array_key_exists('mese_nascita',$dati) ) ? $data_finale['mese_nascita'] = stripslashes($dati['mese_nascita']) : null;

            (array_key_exists('anno_nascita',$dati) ) ? $data_finale['anno_nascita'] = stripslashes($dati['anno_nascita']) : null;

            (array_key_exists('url_facebook',$dati) ) ? $data_finale['url_facebook'] = stripslashes($dati['url_facebook']) : null;

            (array_key_exists('url_linkedin',$dati)) ? $data_finale['url_twitter'] = stripslashes($dati['url_twitter']) : null;

            (array_key_exists('url_twitter',$dati)) ? $data_finale['url_linkedin'] = stripslashes($dati['url_linkedin']) : null;


            return $data_finale;


        } // fine controllo dati


    } // fine metodo Put


} // fine classe saveUserInformazioni