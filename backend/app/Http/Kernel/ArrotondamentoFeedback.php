<?php

namespace App\Http\Kernel;

class ArrotondamentoFeedback { // inizio classe ArrotondamentoFeedback

    public function arrotonda ($feedback_totale) { // inizio metodo arrotonda

        $totale = 0;

        if ($feedback_totale != null && count($feedback_totale) > 0 ) { // iniizo controllo $feedback_totale

            foreach ($feedback_totale as $value) { // inizio ciclo foreach

                $totale = $totale + $value->feedback_totale;

            } // fine ciclo foreach

            $arrotondato = number_format($totale / count($feedback_totale), 1);

            $arrotondato_array = explode('.',$arrotondato);

            if (count($arrotondato_array) > 1) {

                switch ($arrotondato_array[1]) { // inizio switch

                    case '0':
                        $feedback_lavoro = $arrotondato_array[0];
                        break;
                    case '1':
                        $feedback_lavoro = $arrotondato_array[0];
                        break;
                    case '2':
                        $feedback_lavoro = $arrotondato_array[0];
                        break;
                    case '3':
                        $feedback_lavoro = $arrotondato_array[0] . '.5';
                        break;
                    case '4':
                        $feedback_lavoro = $arrotondato_array[0] . '.5';
                        break;
                    case '5':
                        $feedback_lavoro = $arrotondato_array[0] . '.5';
                        break;

                    case '6':
                        $feedback_lavoro = $arrotondato_array[0] . '.5';
                        break;
                    case '7':
                        $feedback_lavoro = $arrotondato_array[0] . '.5';
                        break;

                    case '8':
                        $feedback_lavoro = $arrotondato_array[0] + 1 ;
                        break;

                    case '9':
                        $feedback_lavoro = $arrotondato_array[0] + 1 ;
                        break;

                } // fine switch

            } else  {

                $feedback_lavoro = $arrotondato;
            }


        } else {

            $feedback_lavoro = 0;

        } // fine controllo $feedback_totale


        return $feedback_lavoro;



    } // fine metodo arrotonda




} // fine classe ArrotondamentoFeedback