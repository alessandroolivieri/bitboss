<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecuperaPassword extends Mailable
{ // inizio classe

    use Queueable, SerializesModels;

    public $url;



    public function __construct ($url) {

        $this->url = $url;
    }



    public function build () { // inizio build

        // return $this->view('view.name');


        return $this->from('info@filmgooroo.com')
            ->view('mails.recupera_password');

    } // fine build


} // fine classe
