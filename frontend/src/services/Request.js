import axios from 'axios'

export default { // inizio classe
  data: {
    url: 'http://195.201.140.139/bitboss/backend/api/v1/'
  },
  controlloToken () { // inizio metodo controlloToken
    if (localStorage.getItem('token') === null) {
      return false
    } else {
      return true
    }
  }, // fine metodo controlloToken
  logoutAuth () { // inizio metodo logoutAuth
    if (localStorage.getItem('token') !== null) {
      localStorage.removeItem('token')
      location.reload()
    } else {
      // location.reload()
    }
  }, // fine metodo logoutAuth
  SignupAuth (parametri) { // inizio metodo SignupAuth
    let vm = this
    let params = ''
    if (typeof parametri === 'undefined') {
      return Promise.reject(new Error(false))
    } else {
      params = new FormData()
      for (let key in parametri) {
        params.append(key, parametri[key])
      }
    }
    return axios.post(vm.data.url + 'auth/signup', params, {
      headers: {'content-type': 'application/json'}
    })
      .then((response) => { // inizio then data
        return Promise.resolve(response)
      }) // fine then data
      .catch((error) => { // inizio catch error
        return Promise.reject(error.response)
      }) // fine catch error
  }, // fine metodo SignupAuth
  loginAuth (parametri) { // inizio metodo loginAuth
    let vm = this
    let params = ''
    if (typeof parametri === 'undefined') {
      return Promise.reject(new Error(false))
    } else {
      params = new FormData()
      for (let key in parametri) {
        params.append(key, parametri[key])
      }
    }
    return axios.post(vm.data.url + 'auth/login', params, {
      headers: {'content-type': 'application/json'}
    })
      .then((response) => { // inizio then data
        return Promise.resolve(response)
      }) // fine then data
      .catch((error) => { // inizio catch error
        return Promise.reject(error.response)
      }) // fine catch error
  }, // fine metodo loginAuth
  Post (url, parametri) { // inizio apiPost
    const vm = this
    const accessoControllo = vm.controlloToken()
    if (accessoControllo === true) {
      let params = ''
      if (typeof parametri === 'undefined') {
        params = new FormData()
      } else {
        params = new FormData()
        for (let key in parametri) {
          params.append(key, parametri[key])
        }
      }
      return axios.post(vm.data.url + url + '?token=' + localStorage.getItem('token'), params, {
        headers: {'content-type': 'application/json'}
      })
        .then((response) => { // inizio then data
          return Promise.resolve(response)
        }) // fine then data
        .catch((error) => { // inizio catch error
          return Promise.reject(error.response)
        }) // fine catch error
    } else {
      return Promise.reject(new Error(false))
    }
  }, // fine api post
  Get (url, parametri) { // inizio apiGet
    const vm = this
    const accessoControllo = vm.controlloToken()
    if (accessoControllo === true) {
      let urlGet = ''
      if (typeof parametri === 'undefined') {
        urlGet = vm.data.url + url + '?token=' + localStorage.getItem('token')
      } else {
        let parametriGet = ''
        for (let key in parametri) {
          parametriGet = parametriGet + '&' + key + '=' + parametri[key]
        }
        urlGet = vm.data.url + url + '?token=' + localStorage.getItem('token') + parametriGet
      }
      return axios.get(urlGet, {
        headers: {'content-type': 'application/json'}
      })
        .then((response) => {
          return Promise.resolve(response)
        })
        .catch((error) => { // inizio catch error
          if (error.message === 'Network Error') {
            vm.logoutAuth()
            // location.reload()
          }
          // return Promise.reject(new Error(error.response))

          return Promise.reject(error.response)
        }) // fine catch error
    } else {
      return Promise.reject(new Error(false))
    }
  } // fine api get
} // fine classe
