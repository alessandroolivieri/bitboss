import Vue from 'vue'
import Router from 'vue-router'
import Posizioni from '@/components/Posizioni/Posizioni'
import Candidature from '@/components/Candidature/Candidature'
import Candidati from '@/components/Candidati/Candidati'
import HelloWorld from '@/components/HelloWorld'
import NewPosizione from '@/components/Posizioni/NewPosizione'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/posizioni',
      name: 'Posizioni',
      component: Posizioni
    },
    {
      path: '/new_posizione',
      name: 'NewPosizione',
      component: NewPosizione
    },
    {
      path: '/candidature',
      name: 'Candidature',
      component: Candidature
    },
    {
      path: '/candidati',
      name: 'Candidati',
      component: Candidati
    }
  ]
})
